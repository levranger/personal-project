import Header from "./components/Header"
import Search from "./components/Search"
import Content from "./components/Content"
import {useState, useEffect} from "react"
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
const MOVIE_API_URL = "https://www.omdbapi.com/?s=man&apikey=4a3b711b"
function App() {
  const [loading,setLoading] = useState(true)
  const [error,setError] = useState(null)
  const [movies, setMovies] = useState([]);
  const search = (searchValue) => {
    setLoading(true)
    setError(null)
    fetch(`https://www.omdbapi.com/?s=${searchValue}&apikey=4a3b711b`)  
    .then(response=>response.json())
    .then(result => {
      if(result.Response === "True"){
        setMovies(result.Search);
        setLoading(false);
      } else {
        setMovies([])
        setError("404 Not Found")
        setLoading(false)
      }
    })
  }
  useEffect(()=>{
    fetch(MOVIE_API_URL)
    .then(response=>response.json())
    .then(result => {
      if(result.Response === "True"){
        setMovies(result.Search);
        setLoading(false);
      }
    })
  } , [] )

  return (
    <div className="App">
        <Header title="MOVIE SEARCH"/>
        <Search search={search} />
        {loading&& !error?<h1>Loading....</h1>:<Content movies={movies}/>}
      {error?<h3>{error}</h3>:null}
    </div>
  );
  
}

export default App;
