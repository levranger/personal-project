import React, {useState} from "react";
import "./Search.css"
const Search = (props) => {
    const {search} = props;
    const {toggleLoading} = props;
    const [searchValue,setSearchValue] = useState("");
    const handleOnChange = (e) => {
        setSearchValue(e.target.value)
    }
    const resetInput =  () => {
        setSearchValue("")
    }
    const handleEnter = (e) => {
       if(e.keyCode===13){
           search(searchValue)
           resetInput()
    }
}
    return(
        <div className="search-container">
                <input id="search-input" value={searchValue} onKeyUp={handleEnter} onChange={handleOnChange}/> <button onClick={()=>search(searchValue)} onSubmit={()=>search(searchValue)}>SEARCH</button>
        </div>
    )
}
export default Search
